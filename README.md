# Docker - Windows Server - WSLiberty

The scripts and Dockerfiles contain commented out tags for swapping between WSNano and WSCore.  
Edit all the relevant files to use one or the other


Steps to follow:

1) Build the health app .war

	cd .\health\
	mvn clean install
	copy .\target\health.war ..\wlp-health-app\
	cd ..
	
2) Build the WLP docker image

	cd .\wlp-windows\
	.\build.bat

3) Build and run the health docker image

	cd .\wlp-health-config\
	.\build.bat
	cd ..\wlp-health-app\
	.\build.bat
	.\run.bat
	
4) Inspect the running application

	docker logs wlp-health-app
	
Look for the line ( might take a few seconds to start )
[AUDIT   ] CWWKT0016I: Web application available (default_host): http://300081d9c716.local:9080/health/

Put above url in a browser

5) Stop and clean up the running application

	.\remove.bat
	cd ..



	


